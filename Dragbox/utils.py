# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA


def get_icon_for_uri(uri, icon_size=48):
	""" 
	Returns a pixbuf representing the file at
	the URI generally (mime-type based)
	
	@param icon_size: a pixel size of the icon 
	@type icon_size: an integer object.  
	 
	""" 
	from gtk import icon_theme_get_default, ICON_LOOKUP_USE_BUILTIN
	from gnomevfs import get_mime_type
	from gnome.ui import ThumbnailFactory, icon_lookup 

	mtype = get_mime_type(uri)
	icon_theme = icon_theme_get_default()
	thumb_factory = ThumbnailFactory(16)
	icon_name, num = icon_lookup(icon_theme, thumb_factory,  file_uri=uri, custom_icon="")
	icon = icon_theme.load_icon(icon_name, icon_size, ICON_LOOKUP_USE_BUILTIN)
	return icon
	
def run_process(proc, args):
	""" 
	Launches a process
	
	@param proc: The name of the executable 
	@type proc: a string object. 
	
	@param args: A list of arguments to the program 
	@type args: a list object.   
	"""  
	from os import fork, execvp
	args = [proc] + args # strange but true
	pid = fork()
	if not pid:
		try:
			execvp(proc, args)
		except:
			print_error("Failed to run %s %s" % (proc, str(args)))
			raise SystemExit

def quit_dragbox():
	""" 
	terinates the gtk loop and exits 
	""" 
	from gtk import main_quit, main_level
	from sys import exit
	
	# Make sure we output it all 
	from sys import stdout, stderr
	stdout.flush()
	stderr.flush()

	if main_level():
		main_quit()
	else:
		exit()
	
def print_error(string):
	""" 
	Convenience method
	writes an error to stderr, with a special prefix 
	""" 
	string = str(string)
	from sys import stderr
	stderr.write("Error: " + string + "\n")

def print_debug(string):
	""" 
	Convenience method
	writes an debug message to stderr, with a special prefix 
	""" 
	from version import debug_enabled
	if debug_enabled in "yes":
		string = str(string)
		from sys import stderr
		stderr.write("Debug: " + string + "\n")
	
def guess_destdir():
	""" 
	This will find the $DESTDIR if that has been used
	Useful for finding back to datafiles 
	
	Limitation: the binary must be installed "inside" the prefix
	"""
	from os import path
	from sys import argv
	from version import prefix
	pre = path.normpath(path.expanduser(prefix.strip()))
	if pre =="/":
		pre = "/bin" # Trick that works for this special case
	absp = path.normpath(path.abspath(argv[0]))
	datadir_index = absp.find(pre)
	if datadir_index == -1:
		return None
	datadir_guess = absp[:datadir_index]
	print_debug("It seems like DESTDIR is " + datadir_guess)
	return datadir_guess
	
	
	
