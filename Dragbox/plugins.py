# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

"""
Listen to signals from the data controller and provide actions
"""
from sys import stdout

class Writer (object):
	def __init__(self, print_paths, null_terminate):
		self.print_paths = print_paths
		self.null_terminate = null_terminate
	
	def print_item(self, controller, item):
		"""
		Print `item` to stdout
		"""
		item.print_to(stdout, self.print_paths, self.null_terminate)
	
	def print_all(self, controller):
		"""
		Get all items from data controller `controller` and output
		"""
		for item in controller.get_items():
			self.print_item(controller, item)
