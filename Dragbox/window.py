# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

import pygtk
pygtk.require('2.0')
import gtk
import gobject

import preferences
from utils import print_error, print_debug

class ItemView(gtk.Bin):
	"""
	A widget to display one drag item

	Signals:
	activate: parameters: item
	clicked: clicked with other button, parameters: item, event
	close: close button clicked, paramters: item
	dragged: item was dragged successfully, parameters: item, success
	"""
	__gtype_name__ = "ItemView"
	
	def __init__(self, item):
		"""
		Create widget for item
		"""
		super(ItemView, self).__init__()

		self.button = gtk.Button("Placeholder Button", use_underline=False)
		self.button.set_relief(gtk.RELIEF_NONE)
		self.label = self.button.get_children()[0] #this is a label
		self.label.set_justify(gtk.JUSTIFY_LEFT)

		# Create a hbox to contain the dragitem and
		# a close button on the side
		hbox = gtk.HBox(False,0)
		hbox.pack_start(self.button, True, True, 0)
		
		# close button
		closebtn = gtk.Button("")
		image = gtk.Image()
		image.set_from_stock(gtk.STOCK_CLOSE, gtk.ICON_SIZE_MENU)
		image.set_padding(0,0)
		image.set_alignment(0, 0)
		closebtn.set_image(image)
		closebtn.set_relief(gtk.RELIEF_NONE)
		closebtn.connect("clicked", self._cb_close_button, hbox)
		closebtn.set_property("can-focus", False)
		closebtn.set_property("height-request", 22)
		closebtn.set_property("width-request", 22)
		
		alignment = gtk.Alignment()
		alignment.add(closebtn)
		alignment.set(1, 0, 0, 0)
		hbox.pack_start(alignment, False,False,0)

		event_box = gtk.EventBox()
		event_box.add(hbox)

		self.add(event_box)
		self.__child = event_box
		self.__child.show_all()

		self.set_item(item)
		self.setup_signals()
		self.set_as_drag_source()

		tip = self.item.get_description()
		tools = gtk.Tooltips()
		tools.set_tip(event_box, tip)
	
	def setup_signals(self):
		self.button.connect("clicked", self._cb_activate )
		self.button.connect("button-press-event", self._cb_other_click)

	def set_as_drag_source(self):
		""" 
		Make self a drag source
		""" 
		self.button.drag_source_set(gtk.gdk.BUTTON1_MASK,
				self.item.get_targets(),
				gtk.gdk.ACTION_DEFAULT | gtk.gdk.ACTION_COPY | gtk.gdk.ACTION_MOVE | gtk.gdk.ACTION_LINK)
		self.button.connect("drag-data-get", self._cb_drag_send)
		self.button.connect("drag-end", self._cb_drag_end)

	# common widget-children stuff
	def do_size_request (self, requisition):
		requisition.width, requisition.height = self.__child.size_request ()

	def do_size_allocate (self, allocation):
		self.__child.size_allocate (allocation)

	def do_forall (self, include_internals, callback, user_data):
		callback (self.__child, user_data)
	
	def _cb_close_button(self, widget, event):
		self.emit("close", self.item)
	
	def _cb_activate(self, widget):
		self.emit("activate", self.item)
	
	def _cb_other_click(self, widget, event):
		self.emit("clicked", self.item, event)
	
	def _cb_drag_end(self, widget, context):
		""" 
		Called when a drag ends
		""" 
		success = False
		if context.dest_window:	
			# Successful drag
			success = True
		self.emit("dragged", self.item, success)

	def _cb_drag_send(self, widget, context, selection, target_type, event_time):
		""" 
		Write data for drop operation to active selection
		""" 
		self.item.write_to_selection(selection, target_type)
		return True
	
	def get_widget(self):
		return self.button

	def get_item(self):
		return self.item

	def set_item(self, obj):
		"""
		Set the represented object
		"""
		self.item = obj;
		
		def ellipsize(s, width, mark=".."):
			"""
			ellipsize (middle of) string s to width using mark

			for example ellipsize("python for all", 7, "..")
			produces "pyt..ll"
			"""
			if len(s) < width:
				return s
			mark_len = len(mark)
			tail = (width - mark_len)//2
			return u"".join((s[:width - tail - mark_len], mark, s[-tail:]))
		
		icon = self.item.get_icon()
		name = self.item.get_name()
		if icon:
			icon_widget = gtk.Image()
			icon_widget.set_from_pixbuf(icon)
			self.button.set_image(icon_widget)

			line_length = 35
			# work around underline bug
			name = name.replace("_", "__")
		else:
			line_length = 45

		lines = name.split("\n")
		name = u"\n".join(ellipsize(line, line_length) for line in lines)
		self.button.set_label(name)
		
		self.button.set_alignment(0, 0.5)

gobject.type_register(ItemView)
gobject.signal_new("activate", ItemView, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
gobject.signal_new("clicked", ItemView, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT))
gobject.signal_new("close", ItemView, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
gobject.signal_new("dragged", ItemView, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, gobject.TYPE_INT))

class WindowController(object):
	"""
	Dragbox' window controller object

	Listens to signals for added or removed items and
	displays them in the main window. Handles the pop-up
	menu.
	"""
	def __init__(self, controller, window_title):
		self.item_views = {}
		self.item_control = controller
		self.placeholder = None
		self.create_window(window_title)
		
		preferences.notify_add("stick", self.cb_stick_pref_changed)
		controller.connect("item-added", self.added_item)
		controller.connect("item-removed", self.removed_item)
	
	def show_window(self):
		"""
		Moves the window to front
		"""
		self.window.deiconify()
		self.window.present()
		self.window.window.focus()
		
	def ready_window(self):
		"""
		Shows the controlled window
		
		sets window properties
		"""
        # show it all
		self.window.show_all()
		self.adjust_window_size()		
		# Check the sticky preference
		stick = preferences.get_bool("stick", if_none=True)
		if stick:
			self.window.stick()		
		self.full_refresh()
	
	def quit_all(self, ignored=None):
		"""
		terminate the main loop on window destroy event
		"""
		from utils import quit_dragbox
		quit_dragbox()
		
	def create_window(self, name=None):
		"""
		create a new window
		"""
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window_display = self.window.get_display()
		print_debug("Window display is %s" % window_display)
		if not window_display:
			from utils import quit_dragbox
			print_error("Could not get X Display")
			quit_dragbox()

		if name:
			self.window.set_title(name)

		self.window.connect("destroy", self.quit_all)
		self.window.connect("button-press-event", self.cb_window_click)

		self.vbox = gtk.VBox(False, 0)

		# Add a scrollbox around the vbox
		# scrolls the list when it's too long
		scrollbox = gtk.ScrolledWindow()
		scrollbox.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
		scrollbox.set_border_width(0)
		self.window.add(scrollbox)
		scrollbox.add_with_viewport(self.vbox)
		
		# Add placeholder for newbies
		self.make_placeholder()
		pixbuf = self.window.render_icon(gtk.STOCK_DND_MULTIPLE, gtk.ICON_SIZE_DIALOG)
		self.window.set_icon(pixbuf)
		
		# Setup window as a drag TARGET
		from shelfitem import file_targets, text_targets
		self.window.drag_dest_set(gtk.DEST_DEFAULT_ALL, file_targets+text_targets, gtk.gdk.ACTION_DEFAULT | gtk.gdk.ACTION_COPY )
		self.window.connect("drag-data-received", self.cb_drag_data_received)
	
	def adjust_window_size(self, shrink=False):
		"""
		Changes the window size to better fit the content
		
		@param shrink: If the window is allowed to shrink horizontally
		@type shrink: a bool object.
		"""
		#Resize window horizontally
		minwidth = 200
		minheight = 200
		maxheight = 500
		
		win = self.window
		# display new items
		win.show_all()
		scrollbox = self.window.get_child()
		widget = scrollbox.get_child()
		(oldw, oldh) = win.get_size()
		(w, h) = widget.size_request()
		
		if not self.item_views and not self.placeholder:
			# Empty
			self.make_placeholder()
			return
		
		w = max(minwidth, w)
		h = max(minheight, h)
		h = min(maxheight, h)
		
		w = max(w, oldw)
		
		if not shrink:
			vadj = scrollbox.get_vadjustment()
			if h > vadj.page_size:
				vadj.value = vadj.upper - vadj.page_size
				scrollbox.set_vadjustment(vadj)
				widget.queue_draw()
		
		#if not shrink:
		if h < oldh: h = oldh

		# keep old width
		win.resize(w,h)
	
	def make_placeholder(self):
		self.placeholder = gtk.Label("Drag files and text here")
		self.placeholder.set_sensitive(False)
		self.placeholder.show()
		self.vbox.add(self.placeholder)
	
	def added_item(self, ctrl, item):
		self.add_dragitem(item)
	
	def removed_item(self, ctrl, item):
		"""
		Handle dragitem removal
		"""
		child = self.item_views.pop(item)
		self._remove_child(child)

	def _remove_child(self, child, resize=True):
		"""
		Removes a child of the main container widget

		child: the hbox child to be removed, a widget
		"""
		self.vbox.remove(child)
		if resize:
			self.adjust_window_size(shrink=True)
	
	def full_refresh(self):
		"""
		Refresh widget list
		"""
		for view in self.item_views.values():
			self._remove_child(view, resize=False)
		for item in self.item_control.get_items():
			self.add_dragitem(item, lazily=True)
		gobject.timeout_add(50, self.adjust_window_size)
		
	def add_dragitem(self, item, lazily=False):
		"""
		Adds a dragitem's widget to the window's main container

		item: Item to be added
		lazily: if True, don't refresh after adding
		"""
		# Remove placeholder if there
		if self.placeholder:
			self.vbox.remove(self.placeholder)
			self.placeholder = None
				
		handle = ItemView(item)	
		# Add the dragitem & close package to main container
		self.vbox.pack_start(handle, False, True, 0)
		
		self.item_views[item] = handle
		self._add_dragitem_callbacks(handle)
		
		# postpone showing added widget until .adjust_window_size
		if not lazily:
			from gobject import timeout_add
			timeout_add(50, self.adjust_window_size)
	
	def _add_dragitem_callbacks(self, handle):
		"""
		Adds callbacks for individual dragitems
		"""
		handle.connect("activate", self.cb_dragitem_click)
		handle.connect("clicked", self.cb_dragitem_other_click)
		handle.connect("dragged", self.cb_dragitem_dragged)
		handle.connect("close", self.cb_close_button)
		
	def cb_drag_data_received(self, widget, drag_context, x, y, selection_data, info, timestamp):
		"""
		Callback when something is dropped on the window
		"""
		source_widget = drag_context.get_source_widget()
		if source_widget:
			# this is an internal drag, do not handle
			return False
			
		if not self.item_control.handle_drop_data(info, selection_data):
			print_error("Drop data not handled")
			return
		drag_context.finish(True, False, timestamp)  #Finished w succ

	def cb_dragitem_dragged(self, widget, item, success):
		"""
		Callback when drags end
		"""
		if not success:	
			return
		# Successful drag
		# check if we should remove anyways
		remove = preferences.get_bool("remove", False)
		if remove:
			self.item_control.remove_item(item)
			return
		
		# Check if the item (file) is still there after move
		# We have to wait for the filesystem to react
		# Make one quick and one slow to catch it all!
		from gobject import timeout_add

		check_valid = lambda wid, item: self.item_control.check_if_valid(item)
		# Wait 0.1s and 3.0s
		timeout_add(100, check_valid, widget, item)
		timeout_add(3000, check_valid, widget, item)

	def cb_close_button(self, widget, item):
		"""
		Called when a drag item's close button is activated
		"""
		self.item_control.remove_item(item)

	def cb_dragitem_click(self, widget, item):
		self.item_control.activate(item)

	def cb_dragitem_other_click(self, widget, item, event):
		"""
		callback when the mouse button is clicked on the window
		handles mouse buttons 2 and 3
		"""
		if event.button is 3:
			return self._popup_menu(widget, event, item)
		# Propagate event
		return False

	def cb_window_click(self, widget, event, user_info=None):
		"""
		Callback from button click in window
		handles popup menu
		"""
		if event.button is 2:
			return self._handle_move_drag(event)
		if event.button is 3:
			return self._popup_menu(widget, event)
		return False

	def _popup_menu(self, widget, event, item=None):
		def prefs(item):
			import preferences_controller
			preferences_controller.shared.show_window()
		def copy_item(item):
			self.item_control.put_on_clipboard(item)
		def remove_item(item):
			self.item_control.remove_item(item)
		def cut_item(item):
			copy_item(item)
			remove_item(item)

		from aboutdialog import show_about_dialog

		std = [
			(gtk.STOCK_PREFERENCES, prefs),
			(gtk.STOCK_ABOUT, show_about_dialog),
			(gtk.STOCK_QUIT, self.quit_all)
			]
		edit_items = [
			(gtk.STOCK_COPY, copy_item),
			(gtk.STOCK_CUT, cut_item),
			(gtk.STOCK_DELETE, remove_item),
			(None, None) # Separator
			]
		if item:
			menu_items = edit_items + std
		else:
			menu_items = std
		menu = _make_menu(menu_items, item)

		widget.grab_focus()
		# Right click
		menu.popup(None,None,None, event.button, event.time)
		return True

	def _handle_move_drag(self, event):
		(x,y) = event.get_root_coords()
		self.window.begin_move_drag(event.button, int(x), int(y), event.time)
		return True
	
	def cb_stick_pref_changed(client, connection, entry, user_info):
		"""
		Callback from stick preference change
		
		@param client: gconf_client
		@type client: an object.
		
		@param connection:
		@type connection: an object.
		
		@param entry: The gconf entry
		@type entry: a string object.
		"""
		from utils import print_debug
		print_debug("In window sticky callback")
		self.window.set_sticky(client.get_bool(entry))

def _make_menu(items, user_info):
	cmenu = gtk.Menu()
	# Create the menu items
	for (name, callback) in items:
		menu_item = gtk.ImageMenuItem(name)
		cmenu.append(menu_item)
		if callback:
			menu_item.connect_object ("activate", callback, user_info)
	
	cmenu.show_all()
	return cmenu
