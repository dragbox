# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA 

"""
Defines the data item FileItem, TextItem; each representing
one piece of shelf data.
"""

FILE_TYPE = "file"
TEXT_TYPE = "text"
DATA_TYPE = "data"

TARGET_TYPE_TEXT = 80
TARGET_TYPE_URI_LIST = 81

from gtk import target_list_add_text_targets, target_list_add_uri_targets
text_targets = target_list_add_text_targets([], TARGET_TYPE_TEXT)
file_targets = target_list_add_uri_targets([], TARGET_TYPE_URI_LIST)

def make_item(obj, type):
	if type == FILE_TYPE:
		item = FileItem(obj)
	else:
		item = TextItem(obj)
	return item

class Item (object):
	maxtiplen = 512
	def __init__(self, obj, type):
		self.object = obj
		self.type = type
	
	def get_type(self):
		return self.type
	
	def get_object(self):
		return self.object
	
	def get_description(self):
		"""
		Return a long description

		Normally this is the tooltip text
		"""
		tip = self.object
		if len(tip) > self.maxtiplen:
			message = "\nContinued: %.1f KiB text total" % (len(tip)/1024.0)
			tip = tip[:self.maxtiplen] + "  [...]" + message
		return tip

	def get_name(self):
		"""
		Return short description
		"""
		pass

	def write_to_selection(self, selection, target_type):
		"""
		Tells the dragitem to write its representation to
		the selection with the type target_type
		"""
		if target_type == TARGET_TYPE_TEXT:
			selection.set_text(self.object, -1)
		else:
			from utils import print_error
			print_error("error with drag types?")

	def get_icon(self):
		"""
		Return an icon as a pixbuf or None
		"""
		return None

	def is_valid(self):
		return True

	def get_human_readable(self):
		"""
		Returns the preferred readable form of the dragitem
		"""
		return str(self.object)
	def _print_self(self, text, stream, null_terminate):
		"""
		print `text` using the specified line ending

		null_terminate: True if should be terminated with \0 (else \n)
		"""
		if null_terminate:
			end = "\0"
		else:
			end = "\n"
		stream.write(text + end)

class TextItem (Item):
	def __init__(self, obj):
		super(TextItem, self).__init__(obj, TEXT_TYPE)

	def get_name(self):
		lines = self.object.strip().split("\n", 3)
		return '"' + "\n".join(lines[:3]) + '"'

	def get_targets(self):
		return text_targets

	def print_to(self, stream, print_path=False, null_terminate=False):
		"""
		Print the represented object
		"""
		self._print_self(self.object, stream, null_terminate)
		
class FileItem (Item):
	def __init__(self, obj):
		super(FileItem, self).__init__(obj, FILE_TYPE)

	def get_name(self):
		from os.path import split
		# find basename of file and display it
		(pa, item) = split(self.object)
		if not item: item = pa;
		return item

	def get_icon(self):
		from utils import get_icon_for_uri
		uri = self.get_uri()
		icon = get_icon_for_uri(uri, icon_size=48)
		return icon
	
	def get_targets(self):
		return text_targets + file_targets

	def write_to_selection(self, selection, target_type):
		"""
		Tells the dragitem to write its representation to
		the selection with the type target_type
		"""
		if target_type == TARGET_TYPE_URI_LIST:
			uri = self.get_uri()
			out = [uri]
			
			selection.set_uris(out)
		else:
			return super(FileItem, self).write_to_selection(selection, target_type)
			
	def print_to(self, stream, print_path=False, null_terminate=False):
		"""
		Print the represented object
		"""
		if print_path:
			text = self.object
		else:
			text = self.get_uri()
		self._print_self(text, stream, null_terminate)

	def get_uri(self):
		"""
		Returns the uri of the dragitem
		Throws if not FILE_TYPE
		"""
		from gnomevfs import get_uri_from_local_path
		uri = get_uri_from_local_path(self.object)
		return uri

	def is_valid(self):
		"""
		Checks if the represented file still exists
		returns 0 if it does not exist
		"""
		uri = self.get_uri()
		from gnomevfs import exists
		return exists(uri)

