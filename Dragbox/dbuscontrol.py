# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

class Error(Exception):
	pass

class ServiceUnavailable(Error):
	pass

class NotEnabledError(ImportError, Error):
	pass

def _clear_dbus_environment():
	"""
	This is an evil run-on-import function to reset
	dbus' environment

	Here we try unsetting DBUS_SESSION_BUS_ADDRESS to
	make dbus recover it from the current X session.
	(debian bug #475448)
	"""
	from os import unsetenv, getenv
	from utils import print_debug
	bus_address_env_name = "DBUS_SESSION_BUS_ADDRESS"
	bus = None

	print_debug("Unsetting %s, to defend against stale environment" %
			bus_address_env_name)
	unsetenv(bus_address_env_name)

_clear_dbus_environment()
try:
	import dbus
	from dbus.mainloop.glib import DBusGMainLoop
	DBusGMainLoop(set_as_default=True)
	from dbus.gobject_service import ExportedGObject
except:
	raise NotEnabledError("Dbus not available")

import gobject

default_object_path = "/default"
service_interface = "org.gna.DragboxInterface"
default_service_name = "org.gna.dragbox"

class Service(ExportedGObject):
	"""
	Provides a dbus server controller

	Exported interface methods:
	activate - sends signal activate
	send_files - sends signal files-received
	send_texts - sends signal texts-received
	
	send_file, send_text: Send single objects. These
	exported methods are for external methods to simplify
	their interaction with dragbox.

	data-requested signal: parameters send_callback and error_callback
	on error, call error_callback with an exception instance. otherwise
	send_callback should be called with two lists: datas, types.
	"""
	__gtype_name__ = "Service"

	@dbus.service.method(service_interface)
	def send_file(self, f):
		"""
		f: absolute path
		"""
		self.send_files([f])
		
	@dbus.service.method(service_interface)
	def send_files(self, data):
		"""
		data: list of absolute paths
		"""
		self.emit("files-received", data)
	
	@dbus.service.method(service_interface)
	def send_text(self, t):
		"""
		t: text string
		"""
		self.send_texts([t])
			
	@dbus.service.method(service_interface)
	def send_texts(self, data):
		"""
		data: list of text strings
		"""
		self.emit("texts-received", data)
	
	@dbus.service.method(service_interface)
	def activate(self):
		"""
		activate application (bring to front)
		"""
		self.emit("activate")
	
	@dbus.service.method(service_interface, out_signature="asas",
			async_callbacks=("send_callback", "error_callback"))
	def get_data(self, send_callback, error_callback):
		"""
		activate application (bring to front)
		"""
		self.emit("data-requested", send_callback, error_callback)

# See Service class declaration for
# documentation of the signals
gobject.type_register(Service)
gobject.signal_new("files-received", Service, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
gobject.signal_new("texts-received", Service, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, ))
gobject.signal_new("activate", Service, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, ())
gobject.signal_new("data-requested", Service, gobject.SIGNAL_RUN_LAST,
		gobject.TYPE_BOOLEAN, (gobject.TYPE_PYOBJECT, gobject.TYPE_PYOBJECT))

def make_service(identifier=None):
	"""
	Return a service object

	identifier: The service identifier, or None to use the default
	"""
	service_name = _make_service_name(identifier)
	session_bus = dbus.SessionBus()
	bus_name = dbus.service.BusName(service_name, bus=session_bus)
	# return an instance of the Service
	return Service(bus_name, object_path=default_object_path)

def _make_service_name(identifier):
	"""
	Make a service name from the string identifier

	Return default service name if identifier is None
	"""
	if identifier:
		accept = "abcdefghijklmnopqrstuvwxyz"
		name = "".join(c for c in identifier.lower() if c in accept)
		identifier = name
	if not identifier:
		return default_service_name
	else:
		return default_service_name + "." + identifier

def _get_dbus_interface(bus):
	"""
	Return the interface of the main dbus object on the given bus
	"""
	dbus_name = 'org.freedesktop.DBus'
	dbus_object_path = '/org/freedesktop/DBus'
	dbusobj =  bus.get_object(dbus_name, dbus_object_path)
	return dbus.Interface(dbusobj, dbus_name)

def _get_dbus_session_bus():
	"""
	Get the dbus SessionBus

	Raises ServiceUnavailable if dbus is not available
	"""
	from utils import print_debug
	bus = None

	try:
		bus = dbus.SessionBus()
	except:
		raise ServiceUnavailable("Could not get dbus session bus")
	return bus

def list_names():
	"""
	Yield available dragbox services by identifier
	"""
	bus = _get_dbus_session_bus()
	iface = _get_dbus_interface(bus)
	bus_names = iface.ListNames()
	for name in bus_names:
		if name.startswith(default_service_name):
			if name == default_service_name:
				yield "(default)"
			else:
				basename = name.replace(default_service_name + ".", "")
				yield basename

class Connection(object):
	"""
	Tries to connect to previously started Service
	"""
	def __init__(self, identifier=None):
		"""
		Create a connection to a running instance

		identifier: service identifier, or None to use the default

		Throws ServiceUnavailable if no service found
		"""
		bus = _get_dbus_session_bus()

		service_name = _make_service_name(identifier)
		# Check with dbus if service is available
		dbusiface = _get_dbus_interface(bus)
		if dbusiface.NameHasOwner(service_name):
				proxy_obj = bus.get_object(service_name, default_object_path)
				if proxy_obj:
					self.iface = dbus.Interface(proxy_obj, service_interface)
					return
		self.iface = None
		raise ServiceUnavailable("%s not found" % service_name)

	def get_interface(self):
		"""
		Returns the dbus interface to the "Service"
		"""
		return self.iface

