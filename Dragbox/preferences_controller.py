# Copyright (C) 2006 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

from gtk.glade import XML
from os import path
from version import glade_directory
import preferences
from utils import print_debug

class preferences_controller(object):
	"""
	Controller to control the preferences window
	Loads the window, listens to signals and saves/changes preferences
	"""
	
	
	def __init__(self):
		"""
		Set up the preferences window
		
		Find the glade file as it was installed, or search for custom
		DESTDIR or check if not installed at all.
		"""
		glade_filename = "preferences.glade"
		glade_file = path.normpath(path.join(glade_directory, glade_filename))

		if not path.exists(glade_file):
			# Try to find if installed with DESTDIR
			from utils import guess_destdir
			while glade_file[0] == "/":
				glade_file = glade_file[1:]
			destdir = guess_destdir()
			if destdir:
				glade_file = path.join(destdir, glade_file)
				print_debug(glade_file)
		if not path.exists(glade_file):
			# try to find if not installed
			from sys import argv
			prog_location = path.abspath(argv[0])
			(head, tail) = path.split(prog_location)
			glade_file = path.join(head, "data", glade_filename)
			print_debug(glade_file)
			
		print_debug("Loading %s" % glade_file)
		try:
			self.ui = XML(glade_file)
		except:
			from utils import print_error
			print_error("Failed to load UI file for preferences window!")
			print_error("File: %s" % glade_file)
			self.win = None
			return
			
		self.win = self.ui.get_widget("prefswindow")
		self.check_stick = self.ui.get_widget("check_stick")
		self.check_open = self.ui.get_widget("check_open")
		self.check_remove = self.ui.get_widget("check_remove")
		
		# signals from .glade file
		signals = {
			"on_check_stick_toggled" : self.cb_stick_toggled,
			"on_check_open_toggled" : self.cb_open_toggled,
			"on_close_clicked" : self.cb_close_clicked,
			"on_check_remove_toggled" : self.cb_remove_toggled,
			"on_prefswindow_delete_event" : self.cb_delete_event,
		}
		# connect signals
		self.ui.signal_autoconnect(signals)
		
		# Set check boxes to prefs values
		stick = preferences.get_bool("stick", if_none=True)
		self.check_stick.set_active(stick)
		
		open = preferences.get_bool("open", if_none=False)
		self.check_open.set_active(open)
		
		remove = preferences.get_bool("remove", if_none=False)
		self.check_remove.set_active(remove)
		
		# set window icon
		from gtk import STOCK_DND_MULTIPLE, ICON_SIZE_DIALOG 
		pixbuf = self.win.render_icon(STOCK_DND_MULTIPLE, ICON_SIZE_DIALOG)
		self.win.set_icon(pixbuf)
		
		# hide window -- only show it if asked for
		self.win.hide()
		
	def show_window(self):
		""" 
		Shows the preferences window 
		""" 
		if not self.win:
			return
		self.win.show_all()
		self.win.present()
	def close_window(self): 
		""" 
		Hides the preferences window 
		""" 
		self.win.hide()
	
	def cb_delete_event(self, *args): 
		# make sure the window is NOT deleted
		self.close_window()
		return True 
		
	def cb_close_clicked(self, widget): 
		self.close_window()

	def cb_stick_toggled(self, widget): 
		val = widget.get_active()
		preferences.set_bool("stick", val)

	def cb_open_toggled(self, widget): 
		val = widget.get_active()
		preferences.set_bool("open", val)
	
	def cb_remove_toggled(self, widget): 
		val = widget.get_active()		 
		preferences.set_bool("remove", val) 
		
# Shared instance of the preferences controller
shared = preferences_controller()		
