# Copyright (C) 2006, 2007 Ulrik Sverdrup
#
#   dragbox -- Commandline drag-and-drop tool for GNOME
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

from sys import exit, argv
from os import path, fork

from utils import print_error, print_debug

class dragbox(object):
	"""
	Dragbox application object
	"""
	def __init__(self):
		"""
		Entrance point for the dragbox application

		+ Read commandline
		+ Try dbus connection
		+ Add items
		+ Run
		"""
		# Read commandline switches
		(files, texts, cli_flags) = self.read_opts(argv[1:])

		# Read from stdin
		from sys import stdin
		if not stdin.isatty():
			pipe_text = stdin.read()
			if pipe_text:
				texts += [pipe_text]

		self.cli_flags = cli_flags

		get = self.cli_flags.get("get")
		list_names = self.cli_flags.get("list-names")
		nofork = self.cli_flags.get("no-fork")
		self.identifier = self.cli_flags.get("identifier")

		success = True
		quit_done = False
		if nofork:
			self.dbus_connection = None
		elif list_names:
			self.get_dbus_names()
			quit_done = True
		else:
			self.dbus_connection = self.get_dbus_connection()
			if get:
				success = self.try_get_data()
				quit_done = True
			elif not nofork and self.dbus_connection:
				self.dbus_send(files, texts)
				quit_done = True
			else:
				self.dbus_service = self.start_dbus_service()
				# Fork to disconnect from terminal
				pid = fork()
				if pid:
					quit_done = True
		if quit_done:
			raise SystemExit(not success)

		(self.data_controller, self.writer) = self.init_data()
		self.add_files(None, files)
		self.add_texts(None, texts)
		self.run_window()

	def init_data(self):
		"""
		Create the data controller and a connected plugins.Writer

		return a tuple of data controller and writer
		"""
		from data import DataController
		data_controller = DataController()

		quiet = self.cli_flags.get("quiet", True)
		onexit = self.cli_flags.get("write-on-exit", False)
		if onexit or not quiet:
			writer = self.make_writer()
			if not quiet:
				data_controller.connect("item-added", writer.print_item)
		else:
			writer = None
		return (data_controller, writer)

	def make_writer(self):
		"""
		Return a plugins.Writer object

		Initialized with current null termination and
		paths/uris settings
		"""
		from plugins import Writer
		null_term = self.cli_flags.get("null", False)
		paths = self.cli_flags.get("paths", True)
		writer = Writer(paths, null_term)
		return writer
	
	def run_window(self):
		"""
		Retrieve the window controller, ready it and run the mainloop
		"""
		from window import WindowController
		
		window_title = self.cli_flags.get("window-title", "Dragbox")
		if self.identifier:
			window_title += (" (%s)" % self.identifier)
		self.wc = WindowController(self.data_controller, window_title)
		self.wc.ready_window()

		try:
			from gtk import main
			main()
		except KeyboardInterrupt:
			# catch ctrl-c and exit nicely
			pass
		self.prepare_quit()
		# quit
	
	def prepare_quit(self):
		exit = self.cli_flags.get("write-on-exit")
		if self.writer and exit:
			self.writer.print_all(self.data_controller)
	
	def read_opts(self, argv):
		"""
		Uses gnu getopt to read command line arguments

		returns a tuple of (files, texts, options)
		"""
		from getopt import gnu_getopt, GetoptError
		short_opts = "hf:t:nvpu0axm:"
		long_opts = [
			'help',
			'file=',
			'text=',
			'no-fork',
			'version',
			'paths',
			'uris',
			'null-terminate',
			'window-title=',
			'write-on-exit',
			'write-async',
			'get',
			'name=',
			'list'
			]
		try:
			(options, arguments) = gnu_getopt(argv, short_opts, long_opts)
		except GetoptError, exception:
			print_error("%s" % exception)
			self.print_usage(usage_error=True)
		prefs = {}
		# store command line flags in `prefs`
		# quiet: if True, output async
		# list-names: if True, list running instances
		for (o,a) in options:
			if o in ("-h", "--help"):
				self.print_usage()
			elif o in ("-v", "--version"):
				self.print_version()
			elif o in ("-n", "--no-fork"):
				prefs["no-fork"] = True
				prefs["quiet"] = True
				prefs["write-on-exit"] = True
			elif o in ("-p", "--paths"):
				prefs["paths"] = True
			elif o in ("-u", "--uris"):
				prefs["paths"] = False
			elif o in ("-0", "--null-terminate"):
				prefs["null"] = True
			elif o in ("--window-title"):
				prefs["window-title"] = a
			elif o in ("-x", "--write-on-exit"):
				# don't write _asyncronously_
				prefs["quiet"] = True
				prefs["write-on-exit"] = True
			elif o in ("-a", "--write-async"):
				# don't write _asyncronously_
				prefs["quiet"] = False
				prefs["write-on-exit"] = False
			elif o in ("--get"):
				prefs["get"] = True
			elif o in ("-m", "--name"):
				prefs["identifier"] = a
			elif o in ("--list"):
				prefs["list-names"] = True

		# Handle strongly typed items
		files = []
		texts = []
		for (o, a) in options:
			if o in ("-f", "--file"):
				path = self.check_file_exists(a, warn=True)
				if path: files.append(path)
			elif o in ("-t", "--text"):
				texts.append(a)
		# Handle rest
		# Try if they are files
		text = ""
		for item in arguments :
			path = self.check_file_exists(item, warn=False)
			if path:
				files.append(path)
			else:
				text = text + " " + item
				
		# Take rest as one text block
		if text: texts.append(text)
		return (files, texts, prefs)

	def check_file_exists(self, f, warn=True):
		"""
		Check if a file exists
		
		f: a local path
		warn: If True, print an error message and exit if nonexistant
		"""
		p = path.abspath(f)
		if path.exists(p):
			return p
		if not warn:
			return None
		print_error("The file %s does not exist" % f)
		raise SystemExit(1)
		
	def get_dbus_connection(self):
		"""
		Tries to connect to already running instance
		"""
		try:
			import dbuscontrol
		except ImportError, info:
			print_error(info)
			return None
		
		try:
			dclient = dbuscontrol.Connection(self.identifier)
		except dbuscontrol.ServiceUnavailable, info:
			return None
		return dclient

	def dbus_send(self, files, texts):
		"""
		Sends files, texts and activates
		"""
		# Send data to running Service
		iface = self.dbus_connection.get_interface()
		if files: iface.send_files(files)
		if texts: iface.send_texts(texts)
		iface.activate()
		send_success = True
		print_debug("Sent data to %s" % iface)
	
	def try_get_data(self):
		"""
		Send get_data method to another instance and try to get its data
		"""
		from shelfitem import make_item
		if not self.dbus_connection:
			# dbus is not available or the requested name
			# is simply not there
			name = self.identifier and self.identifier or "Default"
			print_error("Shelf %s not found" % name)
			self.get_dbus_names()
			return False
		datas, types = self.dbus_connection.get_interface().get_data()
		writer = self.make_writer()
		for data, type in zip(datas, types):
			writer.print_item(None, make_item(data, type))
		return True
	
	def data_requested(self, service, send_callback, error_callback):
		"""
		data-requested-callback from the dbus service
		"""
		print_debug("Data requested from %s" % service)
		items = self.data_controller.get_items()
		datas, types = [], []
		for item in items:
			datas.append(item.get_object())
			types.append(item.get_type())
		send_callback(datas, types)

	def start_dbus_service(self):
		"""
		Create a dbus Service object

		Set up signals for the service
		return the created service or None
		"""
		import dbuscontrol
		try:
			dserver = dbuscontrol.make_service(self.identifier)
		except Exception, info:
			from utils import print_debug, print_error
			print_debug(info)
			print_error("Failed to use dbus, might not be available")
			dbus_service = None
		else:
			dbus_service = dserver
			dbus_service.connect("files-received", self.add_files)
			dbus_service.connect("texts-received", self.add_texts)
			dbus_service.connect("activate", self.activate_application)
			dbus_service.connect("data-requested", self.data_requested)
		return dbus_service
 		
	def get_dbus_names(self):
		from dbuscontrol import list_names
		from utils import print_error
		try:
			names = list(list_names())
		except Exception, info:
			print_error(info)
			return
		print "%d shelves available" % len(names)
		if names:
			print " ",
			print u"\n  ".join(names)
		
	def add_files(self, service, flist):
		"""
		Adds a list of files to the dragbox
		
		flist: a list of absolute paths
		"""
		from shelfitem import FILE_TYPE
		
		for f in flist:
			fileloc = path.abspath(f)
			self.data_controller.add_item(fileloc, FILE_TYPE)

	def add_texts(self, service, tlist):
		"""
		Adds a list of text to the dragbox
		
		tlist: a list of strings
		"""
		from shelfitem import  TEXT_TYPE
		
		for text in tlist:
			self.data_controller.add_item(text, TEXT_TYPE)

	def activate_application(self, service):
		"""
		Activate and bring to front
		"""
		self.wc.show_window()
		
	def print_usage(self, usage_error=False):
		usage_string = """
Usage: dragbox [-naxpu0] [--get | --list] [--name id] [item ...]

    item                    add item to dragbox
                            added as file if it is an existing path

    -t, --text "text"       add text
    -f, --file file         add file

    --get                   output contents of a running dragbox
    --list                  list running dragboxes by identifier

    -m, --name name         set identifier
    --window-title title    set window title
    -n, --no-fork           don't fork (also disables intercommunication)

    Output options
    -a, --write-async       write items as they are recieved
    -x, --write-on-exit     write items on exit
    -p, --paths             print paths of dragged-in files (default)
    -u, --uris              print uris of dragged-in files
    -0, --null-terminate    separate output items with \\0, not \\n


    -h, --help              display this help
    -v, --version           display version information
		"""
		print usage_string
		raise SystemExit(usage_error)

	def print_version(self):
		import version
		print "%s %s\n\n%s" % (version.package_name, version.version, version.copyright_info)
		raise SystemExit

