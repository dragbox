#!/bin/sh

# touch some "needed" files
touch ChangeLog

aclocal \
&& automake --add-missing \
&& autoconf
